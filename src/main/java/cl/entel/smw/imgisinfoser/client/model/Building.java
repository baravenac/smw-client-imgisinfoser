package cl.entel.smw.imgisinfoser.client.model;

import java.math.BigDecimal;

public class Building {
    private BigDecimal id;
    private String name;
    private String type;
    private String pais;
    private String region;
    private String comuna;
    private String infraestructura;
    private String contrato;
    private String calle;
    private BigDecimal altura;
    private String tipoSector;
    private String distribucion;
    private String constructionStatus;
    private String fijacion;
    private String acceso;
    private String propietarioAcceso;
    private String fechaEntrega;
    private String proyecto;
    private String propietario;
    private BigDecimal pisos;
    private BigDecimal locationAddress;

    public Building() {
    }

    public BigDecimal getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(BigDecimal locationAddress) {
        this.locationAddress = locationAddress;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getComuna() {
        return comuna;
    }

    public void setComuna(String comuna) {
        this.comuna = comuna;
    }

    public String getInfraestructura() {
        return infraestructura;
    }

    public void setInfraestructura(String infraestructura) {
        this.infraestructura = infraestructura;
    }

    public String getContrato() {
        return contrato;
    }

    public void setContrato(String contrato) {
        this.contrato = contrato;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public BigDecimal getAltura() {
        return altura;
    }

    public void setAltura(BigDecimal altura) {
        this.altura = altura;
    }

    public String getTipoSector() {
        return tipoSector;
    }

    public void setTipoSector(String tipoSector) {
        this.tipoSector = tipoSector;
    }

    public String getDistribucion() {
        return distribucion;
    }

    public void setDistribucion(String distribucion) {
        this.distribucion = distribucion;
    }

    public String getConstructionStatus() {
        return constructionStatus;
    }

    public void setConstructionStatus(String constructionStatus) {
        this.constructionStatus = constructionStatus;
    }

    public String getFijacion() {
        return fijacion;
    }

    public void setFijacion(String fijacion) {
        this.fijacion = fijacion;
    }

    public String getAcceso() {
        return acceso;
    }

    public void setAcceso(String acceso) {
        this.acceso = acceso;
    }

    public String getPropietarioAcceso() {
        return propietarioAcceso;
    }

    public void setPropietarioAcceso(String propietarioAcceso) {
        this.propietarioAcceso = propietarioAcceso;
    }

    public String getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getProyecto() {
        return proyecto;
    }

    public void setProyecto(String proyecto) {
        this.proyecto = proyecto;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    public BigDecimal getPisos() {
        return pisos;
    }

    public void setPisos(BigDecimal pisos) {
        this.pisos = pisos;
    }
}
