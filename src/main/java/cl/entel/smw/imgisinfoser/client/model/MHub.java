package cl.entel.smw.imgisinfoser.client.model;

import java.math.BigDecimal;

public class MHub {
    private BigDecimal id;
    private String type;
    private String name;
    private String pais;
    private String region;
    private String comuna;
    private String calle;
    private BigDecimal altura;
    private String constructionStatus;
    private String fijacion;
    private String acceso;
    private String propietario;
    private String descripcionLugar;
    private BigDecimal piso;
    private String tipoSector;
    private String infraestructura;
    private String propietarioAcceso;
    private String estadoRespartidor;
    private String tipoNodo;
    private BigDecimal locationAddress;

    public MHub() {
    }

    public BigDecimal getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(BigDecimal locationAddress) {
        this.locationAddress = locationAddress;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getComuna() {
        return comuna;
    }

    public void setComuna(String comuna) {
        this.comuna = comuna;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public BigDecimal getAltura() {
        return altura;
    }

    public void setAltura(BigDecimal altura) {
        this.altura = altura;
    }

    public String getConstructionStatus() {
        return constructionStatus;
    }

    public void setConstructionStatus(String constructionStatus) {
        this.constructionStatus = constructionStatus;
    }

    public String getFijacion() {
        return fijacion;
    }

    public void setFijacion(String fijacion) {
        this.fijacion = fijacion;
    }

    public String getAcceso() {
        return acceso;
    }

    public void setAcceso(String acceso) {
        this.acceso = acceso;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    public String getDescripcionLugar() {
        return descripcionLugar;
    }

    public void setDescripcionLugar(String descripcionLugar) {
        this.descripcionLugar = descripcionLugar;
    }

    public BigDecimal getPiso() {
        return piso;
    }

    public void setPiso(BigDecimal piso) {
        this.piso = piso;
    }

    public String getTipoSector() {
        return tipoSector;
    }

    public void setTipoSector(String tipoSector) {
        this.tipoSector = tipoSector;
    }

    public String getInfraestructura() {
        return infraestructura;
    }

    public void setInfraestructura(String infraestructura) {
        this.infraestructura = infraestructura;
    }

    public String getPropietarioAcceso() {
        return propietarioAcceso;
    }

    public void setPropietarioAcceso(String propietarioAcceso) {
        this.propietarioAcceso = propietarioAcceso;
    }

    public String getEstadoRespartidor() {
        return estadoRespartidor;
    }

    public void setEstadoRespartidor(String estadoRespartidor) {
        this.estadoRespartidor = estadoRespartidor;
    }

    public String getTipoNodo() {
        return tipoNodo;
    }

    public void setTipoNodo(String tipoNodo) {
        this.tipoNodo = tipoNodo;
    }
}
