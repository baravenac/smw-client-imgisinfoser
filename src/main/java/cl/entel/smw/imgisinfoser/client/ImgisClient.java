package cl.entel.smw.imgisinfoser.client;

import cl.entel.smw.imgisinfoser.client.model.*;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import javax.ws.rs.core.MediaType;
import java.util.List;

public class ImgisClient {

    private WebResource webResource;
    private static final String MTE_BY_IDMTE = "/imgis/get_TerminalEnclosureByField";
    private static final String BUILDING_BY_IDBUI = "/imgis/get_Building";
    private static final String HUB_BY_IDHUB = "/imgis/get_HubByField";
    private static final String SPLITTER_BY_IDSPL = "/imgis/get_SplitterByField";
    private static final String SPEC_TE = "/imgis/get_SpecTE";
    private static final String ODF_BY_IDODF = "/imgis/get_SplitterByField";
 //   private static final String CTO_BY_SPLITTER_V2 = "/cto/by/splitterv2";

    public ImgisClient(String endpoint) {
        ClientConfig cc = new DefaultClientConfig();
        cc.getClasses().add(JacksonJsonProvider.class);
        Client client = Client.create(cc);
        webResource = client.resource(endpoint);
    }

    public List<Mte> ctoByIdte(String field, String varTE) throws Exception {
        ClientResponse response = webResource.path(MTE_BY_IDMTE).path(field).path(varTE)
             .type(MediaType.APPLICATION_JSON_TYPE)
             .accept(MediaType.APPLICATION_JSON_TYPE)
             .get(ClientResponse.class);
        //
        if (response.getStatus() != 200) {
           throw new Exception("Error invocando servicio");
        }
        //
        return response.getEntity(new GenericType<List<Mte>>() {
        });
    }

    public List<Building> BuiByIdBui(String idBui) throws Exception {
        ClientResponse response = webResource.path(BUILDING_BY_IDBUI).path(idBui)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get(ClientResponse.class);
        //
        if (response.getStatus() != 200) {
            throw new Exception("Error invocando servicio");
        }
        //
        return response.getEntity(new GenericType<List<Building>>() {
        });
    }

    public List<MHub> HubByIdHub(String field, String varHub) throws Exception {
        ClientResponse response = webResource.path(HUB_BY_IDHUB).path(field).path(varHub)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get(ClientResponse.class);
        //
        if (response.getStatus() != 200) {
            throw new Exception("Error invocando servicio");
        }
        //
        return response.getEntity(new GenericType<List<MHub>>() {
        });
    }

    public List<Splitter> SplitterByvarSpl(String field, String varSpl) throws Exception {
        ClientResponse response = webResource.path(SPLITTER_BY_IDSPL).path(field).path(varSpl)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get(ClientResponse.class);
        //
        if (response.getStatus() != 200) {
            throw new Exception("Error invocando servicio");
        }
        //
        return response.getEntity(new GenericType<List<Splitter>>() {
        });
    }

    public List<ResponseGen> SpecTE() throws Exception {
        ClientResponse response = webResource.path(SPEC_TE)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get(ClientResponse.class);
        //
        if (response.getStatus() != 200) {
            throw new Exception("Error invocando servicio");
        }
        //
        return response.getEntity(new GenericType<List<ResponseGen>>() {
        });
    }

    public List<Odf> OdfByvarOdf(String field, String varOdf) throws Exception {
        ClientResponse response = webResource.path(ODF_BY_IDODF).path(field).path(varOdf)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get(ClientResponse.class);
        //
        if (response.getStatus() != 200) {
            throw new Exception("Error invocando servicio");
        }
        //
        return response.getEntity(new GenericType<List<Odf>>() {
        });
    }

//    public List<Mte> ctoBySplitterV2(String cardUrn) throws Exception {
//        ClientResponse response = webResource.path(CTO_BY_SPLITTER_V2).path(cardUrn)
//                .type(MediaType.APPLICATION_JSON_TYPE)
//                .accept(MediaType.APPLICATION_JSON_TYPE)
//                .get(ClientResponse.class);
//        //
//        if (response.getStatus() != 200) {
//            throw new Exception("Error invocando servicio");
//        }
//        //
//        return response.getEntity(new GenericType<List<Mte>>() {
//        });
//    }

    // public PathDetailResponse reroute(String correlation, String rfs,
    //                                   String odfPort, List<NetworkObject> omit,
    //                                   List<NetworkObject> use, Integer ontId) throws CommonJsonException, IOException {
    //     RerouteRequest request = new RerouteRequest(correlation, rfs, odfPort, omit, use, ontId);
    //     ClientResponse response = webResource.path(REROUTE)
    //             .type(MediaType.APPLICATION_JSON_TYPE)
    //             .accept(MediaType.APPLICATION_JSON_TYPE)
    //             .post(ClientResponse.class, request);

    //     if (response.getStatus() != 200) {
    //         throw response.getEntity(CommonJsonException.class);
    //     }

    //     return response.getEntity(PathDetailResponse.class);
    // }

    // public List<Cabinet> getNetworkStructureByUrn(final String urn) throws CommonJsonException {
    //     ClientResponse response = webResource.path(GET_NETWORK_STRUCTURE_BY_URN).path(urn)
    //             .type(MediaType.APPLICATION_JSON_TYPE)
    //             .accept(MediaType.APPLICATION_JSON_TYPE)
    //             .get(ClientResponse.class);

    //     if (response.getStatus() != 200) {
    //         throw response.getEntity(CommonJsonException.class);
    //     }

    //     return response.getEntity(new GenericType<List<Cabinet>>() {
    //     });
    // }

    public static void main(String args[]) throws Exception {
        ImgisClient client = new ImgisClient("http://localhost:8080");
//        List<Long> resultadoV1 = client.ctoBySplitter("swrefVrecordVdatasetZgisVcollectionZmit_cardVkeysZ29600740");
        List<Mte> resultadoV2 = client.ctoByIdte("ID","11155813");
        List<Building> resultadoV3 = client.BuiByIdBui("15419765");
        List<MHub> resultadoV4 = client.HubByIdHub("ID","11237402");
        List<Splitter> resultadoV5 = client.SplitterByvarSpl("ID","29598625");
        List<ResponseGen> resultadoV6 = client.SpecTE() ;
        List<Splitter> resultadoV7 = client.SplitterByvarSpl("ID","26249222");
        System.out.println("Done");
    }
}
