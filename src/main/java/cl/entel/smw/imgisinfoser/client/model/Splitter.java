package cl.entel.smw.imgisinfoser.client.model;

import java.math.BigDecimal;

public class Splitter {

    private BigDecimal id;
    private String name;
    private String specName;
    private String function;
    private String constructionStatus;
    private BigDecimal nivelCascada;
    private BigDecimal contenedorId;
    private String contenedorName;
    private String contenedorSpec;
    private BigDecimal locationAddress;

    public Splitter() {
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecName() {
        return specName;
    }

    public void setSpecName(String specName) {
        this.specName = specName;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getConstructionStatus() {
        return constructionStatus;
    }

    public void setConstructionStatus(String constructionStatus) {
        this.constructionStatus = constructionStatus;
    }

    public BigDecimal getNivelCascada() {
        return nivelCascada;
    }

    public void setNivelCascada(BigDecimal nivelCascada) {
        this.nivelCascada = nivelCascada;
    }

    public BigDecimal getContenedorId() {
        return contenedorId;
    }

    public void setContenedorId(BigDecimal contenedorId) {
        this.contenedorId = contenedorId;
    }

    public String getContenedorName() {
        return contenedorName;
    }

    public void setContenedorName(String contenedorName) {
        this.contenedorName = contenedorName;
    }

    public String getContenedorSpec() {
        return contenedorSpec;
    }

    public void setContenedorSpec(String contenedorSpec) {
        this.contenedorSpec = contenedorSpec;
    }

    public BigDecimal getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(BigDecimal locationAddress) {
        this.locationAddress = locationAddress;
    }
}
