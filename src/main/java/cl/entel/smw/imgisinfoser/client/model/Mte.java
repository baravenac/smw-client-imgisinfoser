package cl.entel.smw.imgisinfoser.client.model;

import java.math.BigDecimal;

public class Mte {

    private BigDecimal id;
    private String name;
    private String pais;
    private String region;
    private String comuna;
    private String calle;
    private BigDecimal altura;
    private BigDecimal specId;
    private String specName;
    private String conSta;
    private String fijacion;
    private String acceso;
    private String fechaEnt;
    private String proy;
    private String prop;
    private BigDecimal locationAddress;

    public Mte() {
    }

    public BigDecimal getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(BigDecimal locationAddress) {
        this.locationAddress = locationAddress;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getComuna() {
        return comuna;
    }

    public void setComuna(String comuna) {
        this.comuna = comuna;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public BigDecimal getAltura() {
        return altura;
    }

    public void setAltura(BigDecimal altura) {
        this.altura = altura;
    }

    public BigDecimal getSpecId() {
        return specId;
    }

    public void setSpecId(BigDecimal specId) {
        this.specId = specId;
    }

    public String getSpecName() {
        return specName;
    }

    public void setSpecName(String specName) {
        this.specName = specName;
    }

    public String getConSta() {
        return conSta;
    }

    public void setConSta(String conSta) {
        this.conSta = conSta;
    }

    public String getFijacion() {
        return fijacion;
    }

    public void setFijacion(String fijacion) {
        this.fijacion = fijacion;
    }

    public String getAcceso() {
        return acceso;
    }

    public void setAcceso(String acceso) {
        this.acceso = acceso;
    }

    public String getFechaEnt() {
        return fechaEnt;
    }

    public void setFechaEnt(String fechaEnt) {
        this.fechaEnt = fechaEnt;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getProp() {
        return prop;
    }

    public void setProp(String prop) {
        this.prop = prop;
    }
}
