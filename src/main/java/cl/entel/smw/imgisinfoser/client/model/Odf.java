package cl.entel.smw.imgisinfoser.client.model;

import java.math.BigDecimal;

public class Odf {

    private BigDecimal id;
    private String name;
    private BigDecimal specId;
    private String specName;
    private String constructionStatus;
    private String ladoRepartidor;
    private String rackName;
    private BigDecimal contenedorId;
    private String contenedorName;
    private String contenedorType;
    private BigDecimal locationAddress;

    public Odf() {
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getSpecId() {
        return specId;
    }

    public void setSpecId(BigDecimal specId) {
        this.specId = specId;
    }

    public String getSpecName() {
        return specName;
    }

    public void setSpecName(String specName) {
        this.specName = specName;
    }

    public String getConstructionStatus() {
        return constructionStatus;
    }

    public void setConstructionStatus(String constructionStatus) {
        this.constructionStatus = constructionStatus;
    }

    public String getLadoRepartidor() {
        return ladoRepartidor;
    }

    public void setLadoRepartidor(String ladoRepartidor) {
        this.ladoRepartidor = ladoRepartidor;
    }

    public String getRackName() {
        return rackName;
    }

    public void setRackName(String rackName) {
        this.rackName = rackName;
    }

    public BigDecimal getContenedorId() {
        return contenedorId;
    }

    public void setContenedorId(BigDecimal contenedorId) {
        this.contenedorId = contenedorId;
    }

    public String getContenedorName() {
        return contenedorName;
    }

    public void setContenedorName(String contenedorName) {
        this.contenedorName = contenedorName;
    }

    public String getContenedorType() {
        return contenedorType;
    }

    public void setContenedorType(String contenedorType) {
        this.contenedorType = contenedorType;
    }

    public BigDecimal getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(BigDecimal locationAddress) {
        this.locationAddress = locationAddress;
    }
}
