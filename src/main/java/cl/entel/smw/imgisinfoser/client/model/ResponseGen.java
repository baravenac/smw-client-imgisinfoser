package cl.entel.smw.imgisinfoser.client.model;

import java.math.BigDecimal;

public class ResponseGen {

    private BigDecimal id;
    private String value;

    public ResponseGen() {
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
